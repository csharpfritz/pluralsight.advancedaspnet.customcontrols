﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="pluralsight.advancedaspnet.customcontrols.Test" %>

<%@ Register Assembly="MyControls" Namespace="MyControls" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
  <%--<link href="Content/bootstrap.min.css" rel="stylesheet" />--%>
  <%--<script src="Scripts/jquery-1.10.2.min.js"></script>--%>
<%--  <script src="Scripts/bootstrap.min.js"></script>
  <script src="Scripts/respond.min.js"></script>--%>
</head>
<body>
  <form id="form1" runat="server">

    <cc1:MyBootstrapMenu runat="server" ID="testMenu" BackColor="Navy" ForeColor="Yellow"
       title="Pluralsight Course" />

  </form>
</body>
</html>

