﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyControls
{
  public class RenderOptions : WebControl
  {

    public override void RenderControl(System.Web.UI.HtmlTextWriter writer)
    {

      writer.WriteLine("<!-- Render Control -->");

      base.RenderControl(writer);

      writer.WriteLine("<!-- End Render Control -->");

    }

    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {

      writer.WriteLine("<!-- Render -->");

      base.Render(writer);

      writer.WriteLine("<!-- End Render -->");

    }

    public override void RenderBeginTag(System.Web.UI.HtmlTextWriter writer)
    {

      writer.WriteLine("<!-- Render Begin Tag -->");

      writer.RenderBeginTag("video");
      //base.RenderBeginTag(writer);

      writer.WriteLine("<!-- End Render Begin Tag -->");
    }

    protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
    {

      writer.WriteLine("<!-- Render Contents -->");

      base.RenderContents(writer);

      writer.WriteLine("<!-- End Render Contents -->");

    }

    protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
    {

      writer.WriteLine("<!-- Render Children -->");

      base.RenderChildren(writer);

      writer.WriteLine("<!-- End Render Children -->");
    }

    public override void RenderEndTag(System.Web.UI.HtmlTextWriter writer)
    {

      writer.WriteLine("<!-- RenderEndTag -->");

      base.RenderEndTag(writer);

      writer.WriteLine("<!-- End RenderEndTag -->");
    }

  }
}
