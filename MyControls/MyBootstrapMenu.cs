﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyControls
{

  public class MyBootstrapMenu : WebControl, INamingContainer
  {

    protected override void OnPreRender(EventArgs e)
    {

      AddScriptsAndStyles();

      base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter tw)
    {

      tw.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
      tw.AddAttribute(HtmlTextWriterAttribute.Class, "navbar navbar-inverse navbar-fixed-top");
      tw.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, ColorTranslator.ToHtml(BackColor));
      tw.RenderBeginTag("div");

      tw.AddAttribute("class", "container");
      tw.RenderBeginTag("div");

      RenderHamburger(tw);
      RenderMenuItems(tw);

      tw.RenderEndTag(); // end div.container

      tw.RenderEndTag();  // end div.navbar

    }

    private void AddScriptsAndStyles()
    {

      var cs = Page.ClientScript;

      var cssUrl = cs.GetWebResourceUrl(
        this.GetType(),
        "MyControls.Styles.bootstrap.min.css"
      );
      var css = new LiteralControl(
        string.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />",
        cssUrl
      ));
      css.ID = "bootstrap_css";
      Page.Header.Controls.Add(css);

      var regJS = new Action<string, string> ((name, script) => 
        cs.RegisterClientScriptInclude(
          name,
          cs.GetWebResourceUrl(
            this.GetType(), 
            script)
          )
        );

      regJS("jquery", "MyControls.Scripts.jquery-1.10.2.min.js");
      regJS("bootstrap", "MyControls.Scripts.bootstrap.min.js");
      regJS("respond", "MyControls.Scripts.respond.min.js");

    }

    /// <summary>
    /// Renders the hamburger menu.
    /// </summary>
    /// <param name="tw">The textwriter</param>
    private void RenderHamburger(HtmlTextWriter tw)
    {

      tw.AddAttribute(HtmlTextWriterAttribute.Class, "navbar-header");
      tw.RenderBeginTag(HtmlTextWriterTag.Div);

      // Three bars in a button
      tw.AddAttribute(HtmlTextWriterAttribute.Type, "button");
      tw.AddAttribute(HtmlTextWriterAttribute.Class, "navbar-toggle");
      tw.AddAttribute("data-toggle", "collapse");
      tw.AddAttribute("data-target", ".navbar-collapse");
      tw.RenderBeginTag(HtmlTextWriterTag.Button);

      for (int i = 0; i < 3; i++)
      {
        tw.AddAttribute("class", "icon-bar");
        tw.RenderBeginTag("span");
        tw.RenderEndTag(); // end SPAN
      }

      tw.RenderEndTag(); // end Button

      RenderTitle(tw);

      tw.RenderEndTag(); // End Div

    }

    /// <summary>
    /// Renders the menu items.
    /// </summary>
    /// <param name="tw">The textwriter</param>
    private void RenderMenuItems(HtmlTextWriter tw)
    {

      Dictionary<string, string> menuItems = new Dictionary<string, string>() {
        {"Home", "~/"},
        {"About", "~/About"},
        {"Contact", "~/Contact"}
      };

      tw.AddAttribute("class", "navbar-collapse collapse");
      tw.RenderBeginTag("div");

      tw.AddAttribute("class", "nav navbar-nav");
      tw.RenderBeginTag("ul");

      foreach (var item in menuItems)
      {

        tw.RenderBeginTag("li");

        tw.AddAttribute("href", ResolveUrl(item.Value));
        tw.RenderBeginTag("a");
        tw.Write(item.Key);
        tw.RenderEndTag(); // end A

        tw.RenderEndTag(); // end LI

      }

      tw.RenderEndTag(); // end UL

      tw.RenderEndTag(); // end div.navbar-nav

    }

    private void RenderTitle(HtmlTextWriter tw)
    {

      // Menu title
      tw.AddAttribute("class", "navbar-brand");
      tw.AddAttribute("href", ResolveUrl("~/"));
      tw.AddStyleAttribute(HtmlTextWriterStyle.Color, ColorTranslator.ToHtml(ForeColor));
      tw.RenderBeginTag("a");
      tw.Write(Title);
      tw.RenderEndTag(); // end A

    }

    [Browsable(true)]
    [DefaultValue("My Application")]
    [Category("Demo Properties")]
    public string Title { get; set; }

  }

}
